from random import choice, randrange
from string import ascii_letters, hexdigits
from pytest import warns, fail
from requests_mock import mock as requests_mock
from requests_mock.exceptions import NoMockAddress
from tfw_myworker.loaders import github_gists_loader, LimitWarning


def github_gist_item(description):
    gist_id = "".join(choice(hexdigits) for _ in range(32))
    username = "".join(choice(ascii_letters) for _ in range(15))

    return {
        "url": "https://api.github.com/gists/",
        "forks_url": "https://api.github.com/gists/{}/forks".format(gist_id),
        "commits_url": "https://api.github.com/gists/{}/commits".format(gist_id),
        "id": "{}".format(gist_id),
        "node_id": "MDQ6R2lzdGRmMWJiNWI3OTA2OTlhOWViNTYzNTM5NDQ3YTM0Y2Fh",
        "git_pull_url": "https://gist.github.com/{}.git".format(gist_id),
        "git_push_url": "https://gist.github.com/{}.git".format(gist_id),
        "html_url": "https://gist.github.com/{}".format(gist_id),
        "files": {
            "test": {
                "filename": "test",
                "type": "application/json",
                "language": "JSON",
                "raw_url": "https://gist.githubusercontent.com/{0}/{1}/raw/{1}/playground.rs"
                    .format(username, gist_id),
                "size": randrange(10000)
            }
        },
        "public": True,
        "created_at": "2019-07-01T17:58:46Z",
        "updated_at": "2019-07-01T17:58:46Z",
        "description": description,
        "comments": 0,
        "user": None,
        "comments_url": "https://api.github.com/gists/{}/comments".format(gist_id),
        "owner": {
            "login": username,
            "id": randrange(100000),
            "node_id": "MDQ6VXNlcjM3MDQ2MTYy",
            "url": "https://api.github.com/users/{}".format(username),
            "html_url": "https://github.com/{}".format(username),
            "followers_url": "https://api.github.com/users/{}/followers".format(username),
            "following_url": "https://api.github.com/users/{}/following/other_user".format(username),
            "gists_url": "https://api.github.com/users/{}/gists/gist_id".format(username),
            "starred_url": "https://api.github.com/users/{}/starred/owner/repo".format(username),
            "subscriptions_url": "https://api.github.com/users/{}/subscriptions".format(username),
            "organizations_url": "https://api.github.com/users/{}/orgs".format(username),
            "repos_url": "https://api.github.com/users/{}/repos".format(username),
            "events_url": "https://api.github.com/users/{}/events/privacy".format(username),
            "received_events_url": "https://api.github.com/users/{}/received_events".format(username),
            "type": "User",
            "site_admin": False
        },
        "truncated": False
    }


class TestGistsLoader(object):
    def test_loader__should_return_all_gists(self):
        gists = [github_gist_item("test")] * 120
        expected_output = [{
            "title": gist["description"],
            "url": gist["files"]["test"]["raw_url"],
            "display_url": gist["url"]
        } for gist in gists]

        with requests_mock() as mock:
            mock.get("https://api.github.com/gists/public?page=1&per_page=70", json=gists[:70])
            mock.get("https://api.github.com/gists/public?page=2&per_page=70", json=gists[70:])

            output = github_gists_loader(140, page_size=70)

            assert output == expected_output, "Gist loader returned wrong data!"

    def test_loader__should_return_limited_list(self):
        limit = 100
        gists = [github_gist_item("test")] * 120
        expected_output = [{
            "title": gist["description"],
            "url": gist["files"]["test"]["raw_url"],
            "display_url": gist["url"]
        } for gist in gists[:limit]]

        try:
            with requests_mock() as mock:
                mock.get("https://api.github.com/gists/public?page=1&per_page=70", json=gists[:70])
                mock.get("https://api.github.com/gists/public?page=2&per_page=30", json=gists[70:100])

                output = github_gists_loader(limit, page_size=70)

                assert output == expected_output, "Gist loader returned wrong data!"
        except NoMockAddress:
            fail("Request size optimisation is broken!")

    def test_loader__should_return_empty_list_on_empty_response(self):
        try:
            with requests_mock() as mock:
                mock.get("https://api.github.com/gists/public?page=1&per_page=70", json=[])

                output = github_gists_loader(100, page_size=70)

                assert output == [], "Gist loader returned wrong data!"
        except NoMockAddress:
            fail("Requests amount optimisation is broken!")

    def test_loader__should_warn_about_requests_limit_and_return_all_data(self):
        gists = [github_gist_item("test")] * 120
        expected_output = [{
            "title": gist["description"],
            "url": gist["files"]["test"]["raw_url"],
            "display_url": gist["url"]
        } for gist in gists]

        with requests_mock() as mock:
            for index in range(1, 61):
                mock.get("https://api.github.com/gists/public?page={}&per_page=2".format(index),
                         json=gists[(index - 1) * 2: index * 2])

            with warns(LimitWarning) as warning:
                output = github_gists_loader(120, page_size=2)
                if not warning:
                    fail("Gist loader do not warn user about unauthorized api limits!")
                assert output == expected_output, "Gist loader returned wrong data!"
