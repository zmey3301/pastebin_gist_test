from os import path
from requests_mock import mock as requests_mock
from pytest import fixture, raises, warns
from tfw_myworker.loaders import pastebin_loader, LimitWarning

CONTENT_DIR = path.join(path.dirname(path.realpath(__file__)), "content")


@fixture
def pastebin_archive():
    with open(path.join(CONTENT_DIR, "pastebin-archive.html")) as html:
        return html.read()


@fixture
def pastebin_empty():
    with open(path.join(CONTENT_DIR, "pastebin-empty.html")) as html:
        return html.read()


class TestPastesLoader(object):
    def test_loader__should_return_all_pastes(self, pastebin_archive):
        expected_list = [
            {
                "url": "https://pastebin.com/raw/kar3E3Np",
                "title": "Untitled",
                "display_url": "https://pastebin.com/kar3E3Np"
            },
            {
                "url": "https://pastebin.com/raw/uJfDB91E",
                "title": "Untitled",
                "display_url": "https://pastebin.com/uJfDB91E"
            },
            {
                "url": "https://pastebin.com/raw/qRFsr5Zc",
                "title": "Untitled",
                "display_url": "https://pastebin.com/qRFsr5Zc"
            },
            {
                "url": "https://pastebin.com/raw/HxdpYtTu",
                "title": "SYALBIS",
                "display_url": "https://pastebin.com/HxdpYtTu"
            }
        ]

        expected_amount = 49

        with requests_mock() as mock:
            mock.get("https://pastebin.com/archive", text=pastebin_archive)

            output = pastebin_loader(49)

            assert len(output) == expected_amount, "Pastebin loader returned {} pastes, expected {}!"\
                .format(len(output), expected_amount)

            assert output[:len(expected_list)] == expected_list, "Pastebin loader returned wrong data!"

    def test_loader__should_return_limited_list(self, pastebin_archive):
        expected_list = [
            {
                "url": "https://pastebin.com/raw/kar3E3Np",
                "title": "Untitled",
                "display_url": "https://pastebin.com/kar3E3Np"
            },
            {
                "url": "https://pastebin.com/raw/uJfDB91E",
                "title": "Untitled",
                "display_url": "https://pastebin.com/uJfDB91E"
            },
            {
                "url": "https://pastebin.com/raw/qRFsr5Zc",
                "title": "Untitled",
                "display_url": "https://pastebin.com/qRFsr5Zc"
            },
            {
                "url": "https://pastebin.com/raw/HxdpYtTu",
                "title": "SYALBIS",
                "display_url": "https://pastebin.com/HxdpYtTu"
            }
        ]

        expected_amount = 4

        with requests_mock() as mock:
            mock.get("https://pastebin.com/archive", text=pastebin_archive)

            output = pastebin_loader(expected_amount)
            assert len(output) == expected_amount, "Pastebin loader returned {} pastes, expected {}!" \
                .format(len(output), expected_amount)

            assert output == expected_list, "Pastebin loader returned wrong data!"

    def test_loader__should_warn_about_scraping_limit_and_return_all_data(self, pastebin_archive):
        with requests_mock() as mock:
            mock.get("https://pastebin.com/archive", text=pastebin_archive)

            with warns(LimitWarning) as warning:
                output = pastebin_loader(999)
                if not warning:
                    "Pastebin loader do not warn user about scraping limits!"
                assert len(output) == 49, "Pastebin loader returned {} pastes, expected {}!" \
                    .format(len(output), 49)

    def test_loader__should_raise_runtime_error_on_empty_table(self, pastebin_empty):
        with requests_mock() as mock:
            mock.get("https://pastebin.com/archive", text=pastebin_empty)

            with raises(RuntimeError):
                pastebin_loader(49)
