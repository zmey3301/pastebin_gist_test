"""
Module Introduction.

Any module level documentation comes here.
"""

import logging
import re
from warnings import warn
from threading import Thread
from queue import Queue
from requests import get as get_request
from requests.exceptions import HTTPError
from tf_workers import (  # pylint: disable=E0611
    Worker, SettingProperty, WorkerResponse, ResponseCodes as RC
)
from tf_workers.config import ApplicationConfig  # pylint: disable=E
from tfw_myworker.loaders import github_gists_loader, pastebin_loader

config = ApplicationConfig.get_config()

log = logging.getLogger(__name__)

REQUEST_CHUNK_SIZE = 8192


class MyWorker(Worker):
    """
    MyWorker.

    Description and list of parameters. For example:

    :param domain_or_ip:
        Required - The domain or IP Address to perform whois and IPWhois on
    """

    name = "myworker"
    resource_requirements = "LOW"
    requires = [
        # any additional python modules come here
        "bs4",
        "lxml",
        "requests"
    ]
    os_requires = [
        # Any additional OS (Linux-Ubuntu) packages come here
    ]

    def _init_settings(self):
        """Initialize worker settings."""
        super()._init_settings()
        # Add any arguments/properties required by the worker here.
        # self.settings.add(SettingProperty(
        #     name="property_name", data_type=str,
        #     description="property description"))

        # Note: These are inputs to the worker. If you want any information
        # passed to the worker it must be added to self.settings

        self.settings.add(
            SettingProperty(
                name="number_of_pates_gists",
                data_type=int,
                description="Amount of last gists/pastes to process"
            )
        )
        self.settings.add(
            SettingProperty(
                name="match_patterns",
                data_type=list,
                description="List of regular expression patterns"
            )
        )

    def __init__(self, **kwargs):
        """Create worker object."""
        super().__init__(kwargs)
        self.response = None
        self.data_queue = None

    def run(self):
        """Call this method to run the WHOIS worker."""
        super().run()
        self.response = WorkerResponse()
        self.response.response_code = RC.SUCCESS
        self.response.data = []
        self.data_queue = Queue()
        results = {}
        patterns = self.settings.match_patterns.value
        limit = self.settings.number_of_pates_gists.value

        processing_thread = Thread(target=self.data_processing,
                                   args=(patterns, self.data_queue, results))
        processing_thread.start()

        try:
            for document in github_gists_loader(limit) + pastebin_loader(limit):
                self.load_document(document)

        except (HTTPError, RuntimeError):
            pass

        self.data_queue.put(None)
        processing_thread.join()

        self.response.data = [{
            "url": url,
            "matches": sorted(list(matched_patterns))
        } for url, matched_patterns in results.items()]

        return self.response

    def load_document(self, document):
        """
        Load document by chunks and push it to data queue
        :param document: current document data (url and title)
        """
        def send_to_queue(line):
            self.data_queue.put((document["display_url"], line))

        send_to_queue(document["title"])

        with get_request(document["url"], stream=True) as request:
            buffer = ""
            request.raise_for_status()
            for chunk in request.iter_content(REQUEST_CHUNK_SIZE):
                try:
                    data_lines = chunk.decode().split("\n")
                except UnicodeDecodeError:
                    warn("Cannot parse file from {} cause of bad encoding, skipping"
                         .format(document["display_url"]), UnicodeWarning)
                    break
                # As last line in chunk may not be full we're caching it and prepending
                # to the first line of the next chunk
                if buffer:
                    data_lines[0] = buffer + data_lines[0]
                buffer = data_lines.pop()
                for data_line in data_lines:
                    send_to_queue(data_line)

            if buffer:
                send_to_queue(buffer)

    @staticmethod
    def data_processing(patterns: list, data_queue: Queue, results: dict):
        """
        Data matching function
        :param patterns: regex patterns list
        :param data_queue:
        :param results: matching results dict
        """
        compiled_patterns = tuple(re.compile(pattern) for pattern in set(patterns))
        while True:
            data_chunk = data_queue.get()

            if data_chunk is None:
                data_queue.task_done()
                break

            url, data_line = data_chunk
            matched_patterns = set(regex.pattern for regex in compiled_patterns
                                   if regex.match(data_line))
            if url not in results:
                results[url] = matched_patterns
            else:
                results[url] |= matched_patterns

            data_queue.task_done()
