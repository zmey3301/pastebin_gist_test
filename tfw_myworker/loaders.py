from warnings import warn
import requests
from bs4 import BeautifulSoup


class LimitWarning(Warning):
    """Limit warning"""


def github_gists_loader(limit: int, *, page_size: int = 50) -> list:
    """
    Github gists loader (using public api)
    :param limit: limit of gists to load
    :param page_size: amount of gists for one request
    :return: list of loaded gists
    """
    gists = []
    page = 1

    if limit / page_size >= 60:
        warn("We may reach limit of unauthorized requests for gist API!", LimitWarning)

    while len(gists) < limit:
        current_page_limit = min(limit - len(gists), page_size)
        request = requests.get("https://api.github.com/gists/public?page={}&per_page={}"
                               .format(page, current_page_limit))
        request.raise_for_status()

        response = request.json()
        gists_buffer = []
        for gist in response:
            gists_buffer += [{
                "title": gist.get("description", ""),
                "display_url": gist.get("url"),
                "url": gist_file.get("raw_url")
            } for gist_file in gist["files"].values()]

        gists += gists_buffer

        # Breaking loop if no more data lasts in the API
        if len(gists_buffer) < current_page_limit:
            break

        page += 1

    return gists[:limit]


def pastebin_loader(limit: int) -> list:
    """
    Pastebin pastes loader (using bs4)
    :param limit: limit of pastes to load
    :return: list of loaded pastes
    """
    if limit > 49:
        warn("Max limit for pastebin latest pasts scraping is 49!", LimitWarning)
        limit = 49

    base_url = "https://pastebin.com/{}"

    request = requests.get(base_url.format("archive"))
    request.raise_for_status()

    dom = BeautifulSoup(request.content, "lxml")
    data_rows = dom.select("table.maintable tr")[1:limit + 1]

    if not data_rows:
        raise RuntimeError("Could not find recent pastes on the page {}!".format(request.url))

    pastes = []
    for row in data_rows:
        link = row.select_one("td:first-child > a")
        # Splitting id by last backslash in case pastebin will use absolute links instead of
        # relative or will move default paste view
        paste_id = link["href"].strip("/").rsplit("/", 1)[-1]
        pastes.append({
            "title": link.get_text(),
            "url": base_url.format("/".join(("raw", paste_id))),
            "display_url": base_url.format(paste_id)
        })

    return pastes
