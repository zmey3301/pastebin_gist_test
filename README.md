## pastebin_gist_test

Searching pattern in recent Pastebin pastes and GitHub gists test task.

### Description

Worker accepts two parameters:

1. **number_of_pates_gists**: amount of latest pastes/gists to process.
    1. Pastebin max amount is 49, but you also can specify more cause there is no separated param for gists limit. 
        Loader will emit warning in this case. 
    2. Gists max amount is 3000 cause unauthorized api has limit of 60 requests per hour and we're recieving 50 gists per request.
2. **match_patterns**: list of patterns to match documents against (case sensitive).

Worker returns `WorkerResponse` instance. `data` property will contain list of dicts with following keys:

1. **url**: public url of paste/gist.
2. **matches**: sorted list of matched patterns.

### Installation

```bash
# in case you want to use virtualenv
virtualenv venv -p python3
source venv/bin/activate

pip install tf_workers-0.3-py3-none-any.whl
python setup.py install
```

### Testing

```bash
PYTHONPATH=. pytest
```